"""
PortableQDA tests - REFI-QDA 1.5 support package
"""
import sys
sys.path.append(".")
from test_portableqda import *
from test_refi_qda import *

if __name__ == '__main__':
    print("""run tests like this, in the portableQDA directory:
poetry shell
python -m tests
""")