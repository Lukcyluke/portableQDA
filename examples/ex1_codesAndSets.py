# examples/ex1_codesAndSets.py
import portableqda
#look for output in system logging

codebook = portableqda.codebookCls(output="codebook_example.qdc") #create a codebook

# create 3 codes and group them in two sets
for number in range(3):
    error,errorDesc,code = codebook.createElement(
                                        elementCls=portableqda.codeCls,
                                        name=f"code{number}",
                                        sets=["set1","set2"]) #list accepts sets already in codebook o names for new ones

codebook.writeQdcFile() # export the codebook as a REFI-QDA 1.5 compatible QDC file