# examples/ex2_flowControl.py
#
# element creation returns a named tuple (portableqda.resultCls class),  including
# - error level (0/False or integer)
# - error description
# - a pointer to the new element
#
#

import portableqda
#look for output in system logging

r = portableqda.resultCls  #catch errors, for 2 out of 3 different styles

codebook = portableqda.codebookCls(output="portableQDA_example2_codebook.qdc")
#
# style 1) discard any errors and reference to the new element
#
codebook.createElement(elementCls=portableqda.setCls,  # codebook elements are codeCls or setClas
                       name="set1",
                       description="set1 description")
codebook.createElement(elementCls=portableqda.codeCls,  # codebook elements are codeCls or setClas
                       name="code1",
                       description="code1 description")
portableqda.log.info("code1 created")
#
# style 2) catch  errors and save a reference to the new element. 'r' definded in line 14
#
set2 = r(*codebook.createElement(elementCls=portableqda.setCls,  # codebook elements are codeCls or setClas
                       name="set2",
                       description="set2 description")) #  Bear in mind that 'set' is a built-in type
if set2.error:
    # something went wrong during previous operation
    portableqda.log.warning(f"error '{set2.errorDesc}' during set2 creation,  code2 NOT created")
    #raise ValueError(set2.errorDesc)
else:
    code2 = r(*codebook.createElement(elementCls=portableqda.codeCls,
                                    name="code2",
                                    sets=["set1",set2.result], #sets list uses set names or sets objects
                                    description=f"code2 in set {set2.result.name}"))
    portableqda.log.info(f"{code2.result.name} created")
#
# style 3) catch  errors with a simpler syntax
#
code3 = r(*codebook.createElement(elementCls=portableqda.codeCls,
                                    name="code3",
                                    sets=None,
                                    description=f"code3, member of no set"))
if code3.error:
    #
    # something went wrong during previous operation
    #
    raise ValueError(code3.errorDesc)
else:
    code3=code3.result

portableqda.log.info(f"{code3.name} created")
codebook.writeQdcFile()
